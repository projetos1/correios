import { Body, Controller, HttpCode, Post, UsePipes } from '@nestjs/common';
import { AppService } from './app.service';
import { PrecoPrazoRequestDTO } from './dtos/PrecoPrazoRequestDTO';
import { ValidationPipe } from './core/pipes/ValidationPipe';

@Controller('/correios')
export class AppController {
  constructor(private readonly appService: AppService) {
  }

  @Post('/preco-e-prazo')
  @HttpCode(200)
  @UsePipes(new ValidationPipe())
  async precoPrazo(@Body() precoPrazoRequest: PrecoPrazoRequestDTO) {
    return await this.appService.getPrecoPrazo(precoPrazoRequest);
  }
}
