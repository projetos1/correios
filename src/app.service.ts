import { HttpService, Injectable } from '@nestjs/common';
import { PrecoPrazoDTO } from './dtos/PrecoPrazoDTO';
import { PrecoPrazoRequestDTO } from './dtos/PrecoPrazoRequestDTO';
import { FormatoEnum } from './enumerations/FormatoEnum';
import { parse, validate } from 'fast-xml-parser';

@Injectable()
export class AppService {

  constructor(private readonly httpService: HttpService) {
  }

  precoPrazoList: Array<PrecoPrazoDTO> = [];
  urlCorreios = 'http://ws.correios.com.br';

  options = {
    attributeNamePrefix: '@_',
    attrNodeName: 'attr',
    textNodeName: '#text',
    ignoreAttributes: true,
    ignoreNameSpace: false,
    allowBooleanAttributes: false,
    parseNodeValue: true,
    parseAttributeValue: false,
    trimValues: true,
    cdataTagName: '__cdata',
    cdataPositionChar: '\\c',
    parseTrueNumberOnly: false,
    arrayMode: 'strict',
    stopNodes: ['parse-me-as-string'],
  };

  async getPrecoPrazo(precoPrazoRequest: PrecoPrazoRequestDTO): Promise<Array<PrecoPrazoDTO>> {
    this.precoPrazoList = [];

    if (precoPrazoRequest.codigoEmpresa) {
      let servicos = precoPrazoRequest.servicos.join();
      await this.getPrecoEPrazo(precoPrazoRequest, servicos);
    } else {
      const promises = precoPrazoRequest.servicos
        .map(async servico => await this.getPrecoEPrazo(precoPrazoRequest, servico));
      await Promise.all(promises);
    }

    return this.precoPrazoList;
  }

  private getPrecoEPrazo(precoPrazoRequest: PrecoPrazoRequestDTO, servico: string) {
    return this.precoPrazo(precoPrazoRequest, servico).then(async xml => {
      if (xml.data != null) {
        await this.gerarJsonPrecoPrazo(xml.data);
      }
    });
  }

  async precoPrazo(precoPrazoRequest: PrecoPrazoRequestDTO, servico: string): Promise<any> {
    const url = this.urlCorreios.concat('/calculador/CalcPrecoPrazo.asmx/CalcPrecoPrazo?');
    return this.httpService.get(url, {
      params: {
        nCdEmpresa: precoPrazoRequest.codigoEmpresa,
        sDsSenha: precoPrazoRequest.senhaEmpresa,
        nCdServico: servico,
        sCepOrigem: precoPrazoRequest.cepOrigem,
        sCepDestino: precoPrazoRequest.cepDestino,
        nVlPeso: precoPrazoRequest.peso.toString(),
        nCdFormato: AppService.enumValueFormatoEncomenda(precoPrazoRequest.formatoEncomenda),
        nVlComprimento: precoPrazoRequest.comprimento.toString(),
        nVlAltura: precoPrazoRequest.altura.toString(),
        nVlLargura: precoPrazoRequest.largura.toString(),
        nVlDiametro: precoPrazoRequest.diametro.toString(),
        sCdMaoPropria: precoPrazoRequest.maoPropria,
        nVlValorDeclarado: precoPrazoRequest.valorDeclarado,
        sCdAvisoRecebimento: precoPrazoRequest.avisoRecebimento,
      },
    }).toPromise();
  }

  private gerarJsonPrecoPrazo(xml) {
    if (validate(xml) === true) {
      const jsonObj = parse(xml);

      jsonObj.cResultado.Servicos.cServico
        .forEach(servico => this.precoPrazoList.push(new PrecoPrazoDTO(servico)));
    }
  }

  private static enumValueFormatoEncomenda(formatoEncomenda: FormatoEnum) {
    switch (formatoEncomenda) {
      case FormatoEnum.CAIXA:
        return 1;
      case FormatoEnum.ROLO:
        return 2;
      case FormatoEnum.ENVELOPE:
        return 3;
    }
  }
}
