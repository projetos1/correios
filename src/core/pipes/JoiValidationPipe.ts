import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class JoiValidationPipe implements PipeTransform {
  constructor(private schema: Object) {
  }

  transform(value: any, metadata: ArgumentMetadata) {
  }
}