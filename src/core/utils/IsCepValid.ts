import { registerDecorator, ValidationOptions } from 'class-validator';
import * as cep from 'cep-promise';

export function IsCepValid(validationOptions?: ValidationOptions) {
  return function(object: Object, propertyName: string) {
    registerDecorator({
      name: 'IsCepValid',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        async validate(value: any) {
          let retorno = null;
          await cep(value).then(() => retorno = true).catch(() => retorno = false);
          return retorno;
        },
      },
    });
  };
}