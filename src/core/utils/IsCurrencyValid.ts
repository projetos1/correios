import { registerDecorator, ValidationOptions } from 'class-validator';

export function IsCurrencyValid(validationOptions?: ValidationOptions) {
  let currencyRegex = '^\\$?([0-9]{1,3}.([0-9]{3}.)*[0-9]{3}|[0-9]+)(,[0-9][0-9])?$';

  return function(object: Object, propertyName: string) {
    registerDecorator({
      name: 'IsCurrencyValid',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        async validate(value: any) {
          let regexp = new RegExp(currencyRegex);
          return regexp.test(value);
        },
      },
    });
  };
}