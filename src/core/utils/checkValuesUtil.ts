export class CheckValuesUtil {

  static notNull(value: string | undefined): boolean {
    if (value !== null || value.trim() !== '' ||typeof value !== undefined) {
      return true;
    }
    return false;
  }
}