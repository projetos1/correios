export class PrecoPrazoDTO {
  constructor(servico: any) {
    this.codigo = servico.Codigo.toString();
    this.valor = servico.Valor.toString();
    this.prazoEntrega = Number(servico.PrazoEntrega);
    this.valorMaoPropria = servico.ValorMaoPropria.toString();
    this.valorAvisoRecebimento = servico.ValorAvisoRecebimento.toString();
    this.valorDeclarado = servico.ValorValorDeclarado.toString();
    this.entregaDomiciliar = servico.EntregaDomiciliar.toString();
    this.entregaSabado = servico.EntregaSabado.toString();
    this.erro = servico.Erro.toString();
    this.msgErro = servico.MsgErro.toString();
    this.valorSemAdicionais = servico.ValorSemAdicionais.toString();
    this.obsFim = servico.obsFim.toString();
    this.dataMaxEntrega = servico.DataMaxEntrega;
    this.horaMaxEntrega = servico.HoraMaxEntrega;
  }

  codigo: string;
  valor: string;
  prazoEntrega: number;
  valorMaoPropria: string;
  valorAvisoRecebimento: string;
  valorDeclarado: string;
  valorSemAdicionais: string;
  entregaDomiciliar: string;
  entregaSabado: string;
  erro: string;
  msgErro: string;
  obsFim: string;
  dataMaxEntrega: string = null;
  horaMaxEntrega: string = null;
}