import { ArrayNotEmpty, IsArray, IsEnum, IsNotEmpty, Max, Min } from 'class-validator';
import { SimENaoEnum } from '../enumerations/SimENaoEnum';
import { FormatoEnum } from '../enumerations/FormatoEnum';
import { IsCepValid } from '../core/utils/IsCepValid';
import { IsCurrencyValid } from '../core/utils/IsCurrencyValid';

export class PrecoPrazoRequestDTO {

  codigoEmpresa: string;

  senhaEmpresa: string;

  @IsNotEmpty({ message: 'Cep origem não pode ser nulo' })
  @IsCepValid({ message: 'Cep inválido' })
  cepOrigem: string;

  @IsNotEmpty({ message: 'Cep destino não pode ser nulo' })
  cepDestino: string;

  @IsNotEmpty({ message: 'Formato encomenda não pode ser nulo' })
  @IsEnum(FormatoEnum)
  formatoEncomenda: FormatoEnum;

  @IsNotEmpty({ message: 'Peso não pode ser nulo' })
  @Min(1, { message: 'Valor mínimo do peso é 1 quilograma' })
  @Max(50, { message: 'Valor máximo do peso é 50 quilogramas' })
  peso: number;

  @IsNotEmpty({ message: 'Comprimento não pode ser nulo' })
  @Min(15, { message: 'Valor mínimo do comprimento é 15 centímetros.' })
  @Max(105, { message: 'Valor máximo do comprimento é 105 centímetros.' })
  comprimento: number;

  @IsNotEmpty({ message: 'Altura não pode ser nulo' })
  @Min(1, { message: 'Valor mínimo da altura é 1 centímetros.' })
  @Max(105, { message: 'Valor máximo da altura é 105 centímetros.' })
  altura: number;

  @IsNotEmpty({ message: 'Largura não pode ser nulo' })
  @Min(10, { message: 'Valor mínimo da largura é 10 centímetros.' })
  @Max(105, { message: 'Valor máximo da largura é 105 centímetros.' })
  largura: number;

  @IsNotEmpty({ message: 'Diametro não pode ser nulo' })
  diametro: number;

  @IsNotEmpty({ message: 'Mão própria não pode ser nulo' })
  @IsEnum(SimENaoEnum)
  maoPropria: SimENaoEnum;

  @IsNotEmpty({ message: 'Valor declarado não pode ser nulo' })
  @IsCurrencyValid({ message: 'Valor declarado inválido' })
  valorDeclarado: string;

  @IsNotEmpty({ message: 'Aviso Recebimento não pode ser nulo' })
  @IsEnum(SimENaoEnum)
  avisoRecebimento: SimENaoEnum;

  @IsArray()
  @ArrayNotEmpty({ message: 'Código de serviços não pode ser nulo' })
  servicos: Array<string>;

}